package ru.fadeev.tm;

import ru.fadeev.tm.command.CommandExecutor;
import ru.fadeev.tm.service.ServiceFactory;
import ru.fadeev.tm.service.CrudAble;
import ru.fadeev.tm.util.ConsoleHelper;
import ru.fadeev.tm.util.Operation;

public class App {
    public static ServiceFactory serviceFactory = new ServiceFactory();

    public static void main(String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");

        do {
            try {
                String[] managerAndCommand = ConsoleHelper.getManagerAndCommand();
                Operation operation = Operation.getAllowableOperation(managerAndCommand[1]);
                CrudAble services = serviceFactory.getServiceByName(managerAndCommand[0]);
                CommandExecutor.execute(operation, services);
            }
            catch (IllegalArgumentException e){
                System.err.println(e.getMessage());
            }
        }while (true);
    }
}