package ru.fadeev.tm.command;

import ru.fadeev.tm.service.CrudAble;

public class HelpCommand implements Command {

    @Override
    public void execute(CrudAble service) {
        System.out.println("Show all commands.");
        printProjectCommand();
        printTaskCommand();

    }

    private void printProjectCommand() {
        System.out.println(
                "project-clear: Remove all projects.\n" +
                "project-create: Create new project.\n" +
                "project-list: Show all projects.\n"+
                "project-remove: Remove selected project");
    }

    private void printTaskCommand() {
        System.out.println(
                "task-clear: Remove all tasks.\n" +
                "task-create: Create new task.\n" +
                "task-list: Show all tasks.\n"+
                "task-remove: Remove selected task");
    }
}


