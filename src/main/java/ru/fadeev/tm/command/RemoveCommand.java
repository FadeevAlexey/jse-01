package ru.fadeev.tm.command;

import ru.fadeev.tm.service.CrudAble;

public class RemoveCommand implements Command {
    @Override
    public void execute(CrudAble service) {
        service.remove();
    }
}
