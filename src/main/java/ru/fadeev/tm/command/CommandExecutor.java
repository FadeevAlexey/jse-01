package ru.fadeev.tm.command;

import ru.fadeev.tm.util.Operation;
import ru.fadeev.tm.service.CrudAble;

import java.util.HashMap;
import java.util.Map;

public class CommandExecutor {
  private static Map<Operation, Command> allKnownCommandsMap = new HashMap<>();
    static{
        allKnownCommandsMap.put(Operation.CREATE,new CreateCommand());
        allKnownCommandsMap.put(Operation.LIST, new ListCommand());
        allKnownCommandsMap.put(Operation.HELP, new HelpCommand());
        allKnownCommandsMap.put(Operation.REMOVE, new RemoveCommand());
        allKnownCommandsMap.put(Operation.CLEAR, new ClearCommand());
    }

    public static void execute(Operation operation, CrudAble service)  {
        if (operation != null) {
            Map<Operation, Command> allKnownCommandsMap = CommandExecutor.allKnownCommandsMap;
            if (allKnownCommandsMap.containsKey(operation))
                allKnownCommandsMap.get(operation).execute(service);
        }
    }
}
