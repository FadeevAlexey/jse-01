package ru.fadeev.tm.command;

import ru.fadeev.tm.service.CrudAble;

public interface Command {
    void execute(CrudAble service);
}
