package ru.fadeev.tm.service;

import ru.fadeev.tm.App;
import ru.fadeev.tm.util.ConsoleHelper;
import ru.fadeev.tm.manager.Project;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


public class ProjectService implements CrudAble {
    private List<Project> projects = new LinkedList<>();

    @Override
    public void create() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        String projectName = ConsoleHelper.readString();
        projects.add(new Project(projectName));
        System.out.println("[OK]\n");
    }

    @Override
    public void list() {
        System.out.println("[PROJECT LIST]");
        int count = 1;
        for (Project project : projects) {
            System.out.println(String.format("%d. %s", count++, project.getName()));
        }
        System.out.println();
    }

    @Override
    public void clear() {
        TaskService tasks = (TaskService) App.serviceFactory.getServiceByName("task");
        Project.resetId();
        projects = new LinkedList<>();
        tasks.clear();
        System.out.println("[ALL PROJECTS REMOVE]\n");

    }

    @Override
    public void remove() {
        TaskService tasks = (TaskService) App.serviceFactory.getServiceByName("task");
        System.out.println("[PROJECT REMOVE]");
        System.out.println("ENTER NAME");
        String name = ConsoleHelper.readString();

        Iterator<Project> iterator = projects.iterator();
        while (iterator.hasNext()) {
            Project project = iterator.next();
            if (project.getName().equals(name)) {
                tasks.removeTasksByProjectId(project.getId());
                iterator.remove();
                System.out.println("[OK]\n");
                return;
            }
        }
        throw new IllegalArgumentException("Can't find project");
    }

    int getIdByName(String name) {
        for (Project project : projects) {
            if (project.getName().equals(name))
                return project.getId();
        }
        throw new IllegalArgumentException("Can't find project with name: " + name);
    }

    String getNameById(int id) {
        for (Project project : projects) {
            if (project.getId() == id)
                return  project.getName();
        }
        throw new IllegalArgumentException("Can't find project with id: " + id);
    }
}