package ru.fadeev.tm.service;

import ru.fadeev.tm.App;
import ru.fadeev.tm.util.ConsoleHelper;
import ru.fadeev.tm.manager.Task;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TaskService implements CrudAble {
    private List<Task> tasks = new LinkedList<>();

    @Override
    public void create() {
        ProjectService projects = (ProjectService) App.serviceFactory.getServiceByName("project");
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER PROJECT NAME:");
        String projectName = ConsoleHelper.readString();
        System.out.println("ENTER NAME:");
        String taskName = ConsoleHelper.readString();
        tasks.add(new Task(taskName, projects.getIdByName(projectName)));
        System.out.println("[OK]\n");
    }

    @Override
    public void list() {
        System.out.println("[TASK LIST]");
        ProjectService projects = (ProjectService) App.serviceFactory.getServiceByName("project");
        for (int i = 0; i < tasks.size(); i++) {
            Task task = tasks.get(i);
            System.out.println(
                    String.format("%d. %s\tProject: %s", i + 1, task.getName(), projects.getNameById(task.getProjectId())));
        }

        System.out.println();
    }

    @Override
    public void clear() {
        tasks = new LinkedList<>();
        System.out.println("[ALL TASKS REMOVED]\n");
    }

    @Override
    public void remove() {
        ProjectService projects = (ProjectService) App.serviceFactory.getServiceByName("project");
        List<Integer> tasksCollision = new ArrayList<>();
        System.out.println("[TASK REMOVE]");
        System.out.println("ENTER NAME");
        String taskName = ConsoleHelper.readString();

        for (int i = 0; i < tasks.size(); i++) {
            if (tasks.get(i).getName().equals(taskName))
                tasksCollision.add(i);
        }
        if (tasksCollision.size() > 1) {
            System.out.println(("You have " + tasksCollision.size() + " with same name."));
            System.out.println("Enter project name");
            String projectName = ConsoleHelper.readString();

            int index = -1;
            for (int i = 0; i < tasksCollision.size(); i++) {
                Task task = tasks.get(i);
                if (projects.getNameById(task.getProjectId()).equals(projectName)) {
                    index = i;
                    break;
                }
            }
            if (index >= 0)
                tasks.remove(index);
        } else if (tasksCollision.size() == 1)
            tasks.remove((int) tasksCollision.get(0));
        else
            throw new IllegalArgumentException("can't find task");

        System.out.println("[OK]\n");

    }

    public void removeTasksByProjectId(int id) {
        tasks.removeIf(project -> project.getProjectId() == id);
    }
}

