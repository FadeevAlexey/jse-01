package ru.fadeev.tm.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleHelper {
    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public static String readString() {
        String string = null;
        try {
            string = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (string == null || string.isEmpty())
            throw new IllegalArgumentException("Field cannot be empty, try again");
        return string;
    }

    public static String[] getManagerAndCommand(){
        String[] result = readString().split("-");
        if(result.length == 1)
            return new String[]{null,result[0]};
        return result;
    }
}