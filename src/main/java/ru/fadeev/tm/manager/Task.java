package ru.fadeev.tm.manager;

public class Task {
    private String name;
    private int projectId;

    public Task(String name,int projectId) {
        this.name = name;
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public int getProjectId() {
        return projectId;
    }
}


