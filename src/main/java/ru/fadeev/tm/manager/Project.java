package ru.fadeev.tm.manager;

import java.util.Objects;

public class Project {
   private String name;
   private int id;
   private static int counter;

    public Project(String name) {
        this.name = name;
        this.id = ++counter;
    }

    public String getName() {
        return name;
    }

    public static void resetId(){
        counter = 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Project project = (Project) o;
        return id == project.id &&
                name.equals(project.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    public int getId() {
        return id;
    }
}
